﻿using UnityEngine;
using System.Collections;

public class ChangeLookAtTarget : MonoBehaviour {

	public GameObject target; // the target that the camera should look at

	Vector2 mousePos;

	void Start() {
		if (target == null) 
		{
			target = this.gameObject;
			Debug.Log ("ChangeLookAtTarget target not specified. Defaulting to parent GameObject");
		}
	}

	// Called when MouseDown on this gameObject
	void OnMouseDown () {
		mousePos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
	}

	// Called when MouseUp on this gameObject
	void OnMouseUp () {

		// If the mouse was pressed and released at approximately the same spot.
		// This is to differentiate clicking from dragging.
		bool xSame = (Input.mousePosition.x > mousePos.x - 3) && (Input.mousePosition.x < mousePos.x + 3);
		bool ySame = (Input.mousePosition.y > mousePos.y - 3 ) && (Input.mousePosition.y < mousePos.y + 3 );

		if (xSame && ySame) {
			// change the target of the LookAtTarget script to be this gameobject.
			LookAtTarget.target = target;
			//Camera.main.fieldOfView = 60 * target.transform.localScale.x;
		}
	}
}
