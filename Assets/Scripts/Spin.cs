﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {

	public float speed;

	// Use this for initialization
	void Start () {
		
	}

	void FixedUpdate () {
		transform.Rotate (0f, speed * Time.fixedDeltaTime, 0f);
	}
}
