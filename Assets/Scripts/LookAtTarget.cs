﻿using UnityEngine;
using System.Collections;

public class LookAtTarget : MonoBehaviour {

	static public GameObject target; // the target that the camera should look at
	public GameObject startTarget; // the object that is the initial target

	void Start () {
		target = startTarget;
		if (target == null) 
		{
			target = this.gameObject;
			Debug.Log ("LookAtTarget target not specified. Defaulting to parent GameObject");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (target) {
			transform.LookAt (target.transform);

			Camera.main.fieldOfView -= (Input.GetAxis ("Mouse ScrollWheel") * Time.deltaTime * 200); // You can zoom in and out!
			if (Camera.main.fieldOfView < 0.1f) Camera.main.fieldOfView = 0.1f; // You can't zoom in too far...
			if (Camera.main.fieldOfView > 100) Camera.main.fieldOfView = 100; // ...and you can't zoom out too far.

			// Drag to move the camera around
			if (Input.GetMouseButton (0)) {
				transform.RotateAround (target.transform.position, target.transform.up, Input.GetAxis("Mouse X") * 100 * Time.deltaTime);
				transform.RotateAround (target.transform.position, -gameObject.transform.right, Input.GetAxis("Mouse Y") * 100 * Time.deltaTime);

			}
		}
	}
}
